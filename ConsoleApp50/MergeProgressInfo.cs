﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleApp50
{

    public struct MergeProgressInfo
    {
        public int Created;
        public int Updated;
        public int Deleted;
        public int Total;

        public override string ToString()
        {
            return $"Created: {Created} Updated: {Updated} Deleted: {Deleted} Total: {Total}";
        }
    }
}