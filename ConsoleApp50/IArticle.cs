﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{


    public interface IArticle
    {
        int Key { get; }
        string Description { get; }
        decimal Price { get; }
    }
}
