﻿using System.Security.Cryptography;
using System.Collections.Concurrent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{

    public class Article : IArticle
    {
        public int Key { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public static Article From(IArticle other)
        {
            return new Article
            {
                Key = other.Key,
                Description = other.Description,
                Price = other.Price,
            };
        }
    }
}
