﻿using System.Diagnostics;
using System.Data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{
    static class Program
    {
        const string ConnectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=PlaygroundDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        static async Task Main(string[] args)
        {
            var sw = new Stopwatch();

            IArticleRepository art_repository = new SqlArticleRepository(ConnectionString);
            IArticleProcessor art_processor = new ArticleProcessor(art_repository);

            var progress = new SyncProgress<MergeProgressInfo>(MergeProgressChanged);

            var amount = 180000;

            sw.Restart();

            var info = await art_processor.Merge(GenerateArticles(amount), progress);

            sw.Stop();

            Console.WriteLine($"{sw.ElapsedMilliseconds}ms {info.Total / sw.Elapsed.TotalSeconds}rec./sec");

        }

        static int counter;

        private static void MergeProgressChanged(MergeProgressInfo obj)
        {
            if (++counter % 1000 == 0)
            {
                Console.WriteLine(obj);
            }
        }

        static IEnumerable<IArticle> GenerateArticles(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                yield return new Article { Key = i + 1, Description = "", Price = 12.50m, };
            }
        }
    }
}
