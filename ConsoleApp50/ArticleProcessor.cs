﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{

    public class ArticleProcessor : IArticleProcessor
    {
        private readonly IArticleRepository _articleRepository;

        public ArticleProcessor(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository ?? throw new ArgumentNullException(nameof(articleRepository));
        }

        public Task<MergeProgressInfo> Merge(IEnumerable<IArticle> articles)
        {
            if (articles == null)
            {
                throw new ArgumentNullException(nameof(articles));
            }

            return DoMerge(articles);
        }

        public Task<MergeProgressInfo> Merge(IEnumerable<IArticle> articles, IProgress<MergeProgressInfo> progress)
        {
            if (articles == null)
            {
                throw new ArgumentNullException(nameof(articles));
            }

            if (progress == null)
            {
                throw new ArgumentNullException(nameof(progress));
            }

            return DoMerge(articles, progress);
        }

        private async Task<MergeProgressInfo> DoMerge(IEnumerable<IArticle> articles, IProgress<MergeProgressInfo> progress = null)
        {
            MergeProgressInfo progressInfo = default;

            var serverKeys = new HashSet<int>(await _articleRepository.GetAllKeysAsync().ConfigureAwait(false));

            var counter = 0;

            foreach (var article in articles)
            {
                if (serverKeys.Contains(article.Key))
                {
                    var updated = await _articleRepository.UpdateAsync(article).ConfigureAwait(false);
                    serverKeys.Remove(article.Key);
                    if (updated)
                    {
                        progressInfo.Updated += 1;
                    }
                    else
                    {
                    }
                }
                else
                {
                    await _articleRepository.CreateAsync(article).ConfigureAwait(false);
                    progressInfo.Created += 1;
                }

                progressInfo.Total += 1;
                progress?.Report(progressInfo);

                if (++counter % 1000 == 0)
                {
                    Debug.WriteLine(progressInfo);
                }

            }

            foreach (var key in serverKeys)
            {
                await _articleRepository.DeleteAsync(key).ConfigureAwait(false);
                progressInfo.Deleted += 1;
                progressInfo.Total += 1;
                progress?.Report(progressInfo);

                if (++counter % 1000 == 0)
                {
                    Debug.WriteLine(progressInfo);
                }
            }

            await _articleRepository.SaveAsync().ConfigureAwait(false);

            Debug.WriteLine(progressInfo);

            return progressInfo;
        }
    }
}
