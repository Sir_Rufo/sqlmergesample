﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{
    public class SqlArticleRepository : IArticleRepository
    {
        private readonly SqlConnection _connection;
        private SqlCommand _createCommand;
        private SqlCommand _deleteCommand;
        private SqlCommand _updateCommand;
        private SqlTransaction _transaction;

        protected SqlCommand CreateCommand => _createCommand = _createCommand ?? BuildCreateCommand();
        protected SqlCommand DeleteCommand => _deleteCommand = _deleteCommand ?? BuildDeleteCommand();
        protected SqlCommand UpdateCommand => _updateCommand = _updateCommand ?? BuildUpdateCommand();

        private SqlCommand BuildCreateCommand()
        {
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            cmd.CommandText = "INSERT INTO Waren ( ArtNr, Bezeichnung, Preis ) VALUES ( @ArtNr, @Bezeichnung, @Preis )";
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ArtNr", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Bezeichnung", SqlDbType = SqlDbType.VarChar, Size = 100, Direction = ParameterDirection.Input, });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Preis", SqlDbType = SqlDbType.Decimal, Size = 18, Precision = 4, Direction = ParameterDirection.Input, });
            cmd.Prepare();
            return cmd;
        }

        private SqlCommand BuildDeleteCommand()
        {
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            cmd.CommandText = "DELETE FROM Waren WHERE ArtNr = @ArtNr";
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ArtNr", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, });
            cmd.Prepare();
            return cmd;
        }

        private SqlCommand BuildUpdateCommand()
        {
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            cmd.CommandText = "UPDATE Waren SET Bezeichnung = @Bezeichnung, Preis = @Preis WHERE ArtNr = @ArtNr AND ( Bezeichnung <> @Bezeichnung OR Preis <> @Preis )";
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ArtNr", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Bezeichnung", SqlDbType = SqlDbType.VarChar, Size = 100, Direction = ParameterDirection.Input, });
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@Preis", SqlDbType = SqlDbType.Decimal, Size = 18, Precision = 4, Direction = ParameterDirection.Input, });
            cmd.Prepare();
            return cmd;
        }

        private async Task EnsureConnectedAsync()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
            {
                await _connection.OpenAsync();
                _createCommand = null;
                _deleteCommand = null;
                _updateCommand = null;
                _transaction?.Dispose();
                _transaction = null;
            }

            _transaction = _transaction ?? _connection.BeginTransaction();

        }

        public SqlArticleRepository(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public async Task CreateAsync(IArticle article)
        {
            await EnsureConnectedAsync();
            var cmd = CreateCommand;
            cmd.Parameters["@ArtNr"].Value = article.Key;
            cmd.Parameters["@Bezeichnung"].Value = article.Description;
            cmd.Parameters["@Preis"].Value = article.Price;
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task DeleteAsync(int key)
        {
            await EnsureConnectedAsync();
            var cmd = DeleteCommand;
            cmd.Parameters["@ArtNr"].Value = key;
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<ICollection<int>> GetAllKeysAsync()
        {
            var keys = new List<int>();

            await EnsureConnectedAsync();
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            cmd.CommandText = "SELECT ArtNr FROM Waren";
            using (var reader = await cmd.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    keys.Add(reader.GetInt32(0));
                }
            }

            return keys;
        }

        public async Task<IArticle> GetAsync(int key)
        {
            await EnsureConnectedAsync();
            var cmd = _connection.CreateCommand();
            cmd.Transaction = _transaction;
            cmd.CommandText = "SELECT ArtNr, Bezeichnung, Preis FROM Waren WHERE ArtNr = @ArtNr";
            cmd.Parameters.Add(new SqlParameter { ParameterName = "@ArtNr", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, });

            cmd.Parameters["@ArtNr"].Value = key;

            using (var reader = await cmd.ExecuteReaderAsync())
            {
                if (!await reader.ReadAsync())
                {
                    return null;
                }

                return new Article
                {
                    Key = reader.GetInt32(0),
                    Description = reader.GetString(1),
                    Price = reader.GetDecimal(2),
                };
            }
        }

        public Task SaveAsync()
        {
            _transaction.Commit();
            _transaction.Dispose();
            _transaction = null;

            _createCommand = null;
            _deleteCommand = null;
            _updateCommand = null;

            _connection.Close();

            return Task.CompletedTask;
        }

        public async Task<bool> UpdateAsync(IArticle article)
        {
            await EnsureConnectedAsync();
            var cmd = UpdateCommand;

            cmd.Parameters["@ArtNr"].Value = article.Key;
            cmd.Parameters["@Bezeichnung"].Value = article.Description;
            cmd.Parameters["@Preis"].Value = article.Price;
            var affected = await cmd.ExecuteNonQueryAsync();
            return affected == 1;
        }
    }
}
