﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp50
{

    public interface IArticleRepository
    {
        Task<ICollection<int>> GetAllKeysAsync();
        Task<IArticle> GetAsync(int key);
        Task CreateAsync(IArticle article);
        Task<bool> UpdateAsync(IArticle article);
        Task DeleteAsync(int key);

        Task SaveAsync();
    }
}
