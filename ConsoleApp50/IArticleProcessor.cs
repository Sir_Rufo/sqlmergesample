﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleApp50
{
    public interface IArticleProcessor
    {
        Task<MergeProgressInfo> Merge(IEnumerable<IArticle> articles);
        Task<MergeProgressInfo> Merge(IEnumerable<IArticle> articles, IProgress<MergeProgressInfo> progress);
    }
}