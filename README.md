# README #

Test-Tabelle

```SQL
CREATE TABLE [dbo].[Waren] (
    [ArtNr]       INT             NOT NULL,
    [Bezeichnung] VARCHAR (100)   NOT NULL,
    [Preis]       DECIMAL (18, 4) NOT NULL
);
```